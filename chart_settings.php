<html>
	<head>
		<title>Chart Settings Page</title>
		<?php include "headings_import.php"; ?>
		<script type="text/javascript">
			
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "tasks_dropdown_values.php", 
				success: function(data) {
					var mySelect = $('#mySelect');
					$('select').append('<option val="">&nbsp;</option>');
					$.each( data, function( val, text ) {
						$('select').append('<option val="'+val+'">'+text+'</option>');
					});
				}
			});
			jQuery(document).ready(function(){
				
				jQuery(".taskId").change(function(){
					var val = jQuery(this).val();
					if(val != 'task_id'){
						alert("Please select task_id for this field");
						jQuery(this).prop("selectedIndex", 0);
					}
				});
				
				jQuery(".taskName").change(function(){
					var val = jQuery(this).val();
					if(val != 'task_name'){
						alert("Please select task_name for this field");
						jQuery(this).prop("selectedIndex", 0);
					}
				});
				
				jQuery(".startDate").change(function(){
					var val = jQuery(this).val();
					if(val != 'start_date'){
						alert("Please select start_date for this field");
						jQuery(this).prop("selectedIndex", 0);
					}
				});
				
				jQuery(".endDate").change(function(){
					var val = jQuery(this).val();
					if(val != 'end_date'){
						alert("Please select end_date for this field");
						jQuery(this).prop("selectedIndex", 0);
					}
				});
				
				jQuery(".duration").change(function(){
					var val = jQuery(this).val();
					if(val != 'duration'){
						alert("Please select duration for this field");
						jQuery(this).prop("selectedIndex", 0);
					}
				});
				
				jQuery(".percentComplete").change(function(){
					var val = jQuery(this).val();
					if(val != 'percent_complete'){
						alert("Please select percent_complete for this field");
						jQuery(this).prop("selectedIndex", 0);
					}
				});
			})
		</script>
	</head>
	<body>
	<br /><br /><br />
	<div class="container-fluid" >
		<div class="row" id="chart-settings">
			<div class="col-md-12">
			<form class="form-inline">
			  <div class="input-group mb-2 mr-sm-2 mb-sm-0">
				<div class="input-group-addon">Task ID</div>
				<div class="input-group-addon"><select class="taskId" id="mySelect"></select></div>
				<div class="input-group-addon">Field containing the record id of the task in the Gantt Chart</div>
			  </div>
			</form>
			
			<form class="form-inline">
			  <div class="input-group mb-2 mr-sm-2 mb-sm-0">
				<div class="input-group-addon">Task Name</div>
				<div class="input-group-addon"><select class="taskName" id="mySelect"></select></div>
				<div class="input-group-addon">Field containing the task name in the Gantt Chart</div>
			  </div>
			</form>
			
			<form class="form-inline">
			  <div class="input-group mb-2 mr-sm-2 mb-sm-0">
				<div class="input-group-addon">Start Date</div>
				<div class="input-group-addon"><select class="startDate" id="mySelect"></select></div>
				<div class="input-group-addon">Field containing the start date of the registration in the Gantt Chart</div>
			  </div>
			</form>
			
			<form class="form-inline">
			  <div class="input-group mb-2 mr-sm-2 mb-sm-0">
				<div class="input-group-addon">End Date</div>
				<div class="input-group-addon"><select class="endDate" id="mySelect"></select></div>
				<div class="input-group-addon">Field containing the end date of the registration in the Gantt Chart</div>
			  </div>
			</form>
			
			<form class="form-inline">
			  <div class="input-group mb-2 mr-sm-2 mb-sm-0">
				<div class="input-group-addon">Duration</div>
				<div class="input-group-addon"><select class="duration" id="mySelect"></select></div>
				<div class="input-group-addon">Time duration for this specific task</div>
			  </div>
			</form>
			
			<form class="form-inline">
			  <div class="input-group mb-2 mr-sm-2 mb-sm-0">
				<div class="input-group-addon">Percent Completed</div>
				<div class="input-group-addon"><select class="percentComplete" id="mySelect"></select></div>
				<div class="input-group-addon">Field containing the information regarding the percent completed of the task in the Gantt Chart</div>
			  </div>
			</form>
			<button id="loadProject">LOAD PROJECT</button>
			</div>
		</div>
		<div class="row" id="gantt-chart-proper" ">
			<?php
			include "highchart.php";
			?>
		</div>
	</div>
	</body>
</html>