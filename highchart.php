<?php include "headings_import.php"; ?>
<script src="http://code.highcharts.com/stock/highstock.js"></script>
<script type="text/javascript">

jQuery(document).ready(function(){
	jQuery("#gantt-chart-proper").hide();
	jQuery('#loadProject').click(function(){
	   var selected = [];
		$('select').each( function() {
			var theValue = $(this).prop('selectedIndex');
			
			if(theValue==0)
			{
				alert("Please fill all fields with the correct column reference");
				return false;
			}
			else{
				jQuery("#chart-settings").hide();
				jQuery("#gantt-chart-proper").show();
				
				$(function () {

					   var data = {projectId : 1};
					$.ajax({
						type: "POST",
						dataType: "json",
						url: "gantt_chart_values.php", 
						data: data,
						success: function(jsonData) {
							
							var yAxisLabels = [];
							var tasks = [];
							$.each( jsonData, function( index, val ) {
								
								var extractName = val[1];
								yAxisLabels.push(extractName);
								
								var dateObj = new Date(val[3]);
								var month = dateObj.getUTCMonth(); //months from 1-12
								var day = dateObj.getUTCDate();
								var year = dateObj.getUTCFullYear();
								var newdate = year + "/" + month + "/" + day;
								val[3] =  Date.UTC(year,month,day);
								var extractFromDate = val[3];
								
								var dateObj =  new Date(val[4]);
								var month = dateObj.getUTCMonth(); //months from 1-12
								var day = dateObj.getUTCDate();
								var year = dateObj.getUTCFullYear();
								var newdate = year + "/" + month + "/" + day;
								
								val[4] =  Date.UTC(year,month,day);
								var extractToDate = val[4];
								var description = val[8];
								
								var extractedData = {
									name: extractName,
									intervals: [{
										from: extractFromDate,
										to: extractToDate,
										label: description
									}]
								};
								
								tasks.push(extractedData);
							});
							
						
							var series = [];
							$.each(tasks.reverse(), function(i, task) {
								var item = {
									name: task.name,
									data: []
								};
								$.each(task.intervals, function(j, interval) {
									item.data.push({
										x: interval.from,
										y: i,
										label: interval.label,
										from: interval.from,
										to: interval.to
									}, {
										x: interval.to,
										y: i,
										from: interval.from,
										to: interval.to
									});
									
									// add a null value between intervals
									if (task.intervals[j + 1]) {
										item.data.push(
											[(interval.to + task.intervals[j + 1].from) / 2, null]
										);
									}

								});

								series.push(item);
							});

							// create the chart
							var chart = new Highcharts.Chart({
								chart: {
									renderTo: 'highchart'
								},

								title: {
									text: 'Project: U Task Status Monitoring Gantt Chart'
								},

								xAxis: {
									type: 'datetime'
								},

								yAxis: {
									
									categories: yAxisLabels.reverse(),
									tickInterval: 1,            
									tickPixelInterval: 200,
									labels: {
										style: {
											color: '#525151',
											font: '12px Helvetica',
											fontWeight: 'bold'
										},
									   /* formatter: function() {
											if (tasks[this.value]) {
												return tasks[this.value].name;
											}
										}*/
									},
									startOnTick: false,
									endOnTick: false,
									title: {
										text: 'Criteria'
									},
									minPadding: 0.2,
									maxPadding: 0.2,
									   fontSize:'15px'
									
								},

								legend: {
									enabled: false
								},
								tooltip: {
									formatter: function() {
										return '<b>'+ tasks[this.y].name + '</b><br/>' +
											Highcharts.dateFormat('%m-%d-%Y', this.point.options.from)  +
											' - ' + Highcharts.dateFormat('%m-%d-%Y', this.point.options.to); 
									}
								},

								plotOptions: {
									line: {
										lineWidth: 10,
										marker: {
											enabled: false
										},
										dataLabels: {
											enabled: true,
											align: 'left',
											formatter: function() {
												return this.point.options && this.point.options.label;
											}
										}
									}
								},

								series: series

							});		   
						},
						error: function(){
							alert("error");
						}
					});
				});
			}
	   });
	});
});
</script>
<div id="highchart" style="height: 500px"></div>