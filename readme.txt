1. Please run sql dump first to ensure existence of database.
2. Kindly copy and paste all files in your web root folder to ensure its proper display in the browser.
3. Once files are set on the proper folder, open the index.php page with this format in mind: [yourhost]/[yourprojectname]/index.php
	example: http://localhost/ganttsampleproject/index.php
4. For proper rendering of the gantt chart, I suggest that you open this in Chrome for best viewing experience