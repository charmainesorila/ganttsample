<?php
include "dbConnect.php";
?>
<html>
	<head>
		<title>Sample Project</title>
		<?php
		include "headings_import.php";
		?>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-info">
						<div class="panel-heading">
							<h3 class="panel-title">
								The following are sample demo page immitating a behaviour of gantt chart-generating web application
							</h3>
						</div>
						<div class="panel-body">
							<form class="form-inline">
							  <div class="input-group mb-2 mr-sm-2 mb-sm-0">
								<div class="input-group-addon">This page is a paginated table of all the tasks from the tasks table</div>
								<div class="input-group-addon"><a href="list_page.php">Go to List Page</a></div>
								
							  </div>
							</form>
							
							<form class="form-inline">
							  <div class="input-group mb-2 mr-sm-2 mb-sm-0">
								<div class="input-group-addon">This page is a paginated table of all the tasks from the tasks table</div>
								<div class="input-group-addon"><a href="chart_settings.php">Go to Gantt Chart Setup and Display</a></div>
								
							  </div>
							</form>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</body>
</html>




