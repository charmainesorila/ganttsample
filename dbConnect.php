<?php
	include 'dbSettings.php';
	$conn = new mysqli($dbSettings['host'], $dbSettings['username'], $dbSettings['password'],$dbSettings['database']);
	
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	} 
?>