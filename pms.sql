-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 26, 2017 at 05:57 PM
-- Server version: 5.7.15-log
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pms`
--

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `project_id` int(11) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime DEFAULT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`project_id`, `project_name`, `start_date`, `end_date`, `description`) VALUES
(1, 'Wifun', '2017-01-19 16:06:42', NULL, 'just wifun things and stuff'),
(2, 'Sysnet', '2017-01-19 21:49:07', '2017-01-19 21:49:07', 'Some project description for Sysnet goes here');

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `task_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `project_phase` varchar(255) NOT NULL,
  `task_name` varchar(255) NOT NULL,
  `milestone` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `duration` int(11) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `days_overdue` int(11) NOT NULL,
  `task_status` varchar(255) NOT NULL,
  `percent_complete` int(11) NOT NULL,
  `task_priority` varchar(255) NOT NULL,
  `assigned_to` varchar(255) NOT NULL,
  `history_note` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `last_modified_by` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`task_id`, `project_id`, `project_phase`, `task_name`, `milestone`, `description`, `duration`, `start_date`, `end_date`, `days_overdue`, `task_status`, `percent_complete`, `task_priority`, `assigned_to`, `history_note`, `date_created`, `date_modified`, `user_id`, `last_modified_by`) VALUES
(5, 1, 'some phase', 'Miss U App meeting', 'some milestone', 'data gathering for miss U app', 2, '2017-01-01 21:37:08', '2017-01-05 21:37:08', 3, 'started', 25, 'some priority', 'assigned to someone', 'some history note', '2017-01-19 21:37:08', '2017-01-19 21:37:08', 1, 'last modified by someone'),
(6, 1, 'some phase', 'REST API for Estee Lauder req', 'some milestone', 'some description', 2, '2017-01-05 21:37:18', '2017-01-08 21:37:18', 3, 'started', 25, 'some priority', 'assigned to someone', 'some history note', '2017-01-19 21:37:18', '2017-01-19 21:37:18', 1, 'last modified by someone'),
(7, 1, 'some phase', 'Wash baby bottles', 'some milestone', 'Washing of baby bottles on time', 2, '2017-01-08 21:37:26', '2017-01-11 21:37:26', 3, 'started', 25, 'some priority', 'assigned to someone', 'some history note', '2017-01-19 21:37:26', '2017-01-19 21:37:26', 1, 'last modified by someone'),
(8, 1, 'some phase', 'Make coffee', 'some milestone', 'Making of coffee for breakfast, no excuses', 2, '2017-01-11 21:37:34', '2017-01-12 21:37:34', 3, 'started', 25, 'some priority', 'assigned to someone', 'some history note', '2017-01-19 21:37:34', '2017-01-19 21:37:34', 1, 'last modified by someone'),
(9, 1, 'some phase', 'Laundry', 'some milestone', 'Just do the laundry, you know', 2, '2017-01-11 21:37:45', '2017-01-13 21:37:45', 3, 'started', 25, 'some priority', 'assigned to someone', 'some history note', '2017-01-19 21:37:45', '2017-01-19 21:37:45', 1, 'last modified by someone'),
(10, 1, 'some phase', 'Check email', 'some milestone', 'Check email and get rid of the junk ones', 2, '2017-01-12 21:37:55', '2017-01-15 21:37:55', 3, 'started', 25, 'some priority', 'assigned to someone', 'some history note', '2017-01-19 21:37:55', '2017-01-19 21:37:55', 1, 'last modified by someone'),
(11, 1, 'some phase', 'Remove stains', 'some milestone', 'Remove the stains on the tiles', 2, '2017-01-13 21:38:03', '2017-01-15 21:38:03', 3, 'started', 25, 'some priority', 'assigned to someone', 'some history note', '2017-01-19 21:38:03', '2017-01-19 21:38:03', 1, 'last modified by someone'),
(12, 1, 'some phase', 'Bedtime stories', 'some milestone', 'Because you need the baby to get used with books', 2, '2017-01-14 21:38:32', '2017-01-17 21:38:32', 3, 'started', 25, 'some priority', 'assigned to someone', 'some history note', '2017-01-19 21:38:32', '2017-01-19 21:38:32', 1, 'last modified by someone'),
(13, 1, 'some phase', 'I need this', 'some milestone', 'Because of reasons', 2, '2017-01-17 21:44:28', '2017-01-20 21:44:28', 3, 'started', 25, 'some priority', 'assigned to someone', 'some history note', '2017-01-19 21:44:28', '2017-01-19 21:44:28', 1, 'last modified by someone'),
(14, 1, 'some phase', 'I don''t know what Yokai is', 'some milestone', 'And at this point, im too afraid to ask', 2, '2017-01-18 21:47:22', '2017-01-19 21:47:22', 3, 'started', 25, 'some priority', 'assigned to someone', 'some history note', '2017-01-19 21:47:22', '2017-01-19 21:47:22', 1, 'last modified by someone'),
(15, 1, 'some phase', 'hey I just met you', 'some milestone', 'and this is awesome', 2, '2017-01-19 21:47:34', '2017-01-21 21:47:34', 3, 'started', 25, 'some priority', 'assigned to someone', 'some history note', '2017-01-19 21:47:34', '2017-01-19 21:47:34', 1, 'last modified by someone'),
(16, 1, 'some phase', 'so here''s my number', 'some milestone', 'so call me maybe', 2, '2017-01-20 21:47:42', '2017-01-23 21:47:42', 3, 'started', 25, 'some priority', 'assigned to someone', 'some history note', '2017-01-19 21:47:42', '2017-01-19 21:47:42', 1, 'last modified by someone'),
(17, 1, 'some phase', 'california girls', 'some milestone', 'are undeniable', 2, '2017-01-23 21:47:49', '2017-01-25 21:47:49', 3, 'started', 25, 'some priority', 'assigned to someone', 'some history note', '2017-01-19 21:47:49', '2017-01-19 21:47:49', 1, 'last modified by someone'),
(18, 1, 'some phase', 'moon lovers', 'some milestone', 'Scarlet Heart Ryeo is an awesome KDrama', 2, '2017-01-25 21:47:59', '2017-01-29 21:47:59', 3, 'started', 25, 'some priority', 'assigned to someone', 'some history note', '2017-01-19 21:47:59', '2017-01-19 21:47:59', 1, 'last modified by someone'),
(19, 2, 'some phase', 'some task name21', 'some milestone', 'some description here is okay', 2, '2017-01-25 21:49:38', '2017-01-27 21:49:38', 3, 'started', 25, 'some priority', 'assigned to someone', 'some history note', '2017-01-19 21:49:38', '2017-01-19 21:49:38', 1, 'last modified by someone'),
(20, 2, 'some phase', 'some task name22', 'some milestone', 'the quick brown fox jumped over the lazy dog', 2, '2017-01-28 21:50:05', '2017-01-31 21:50:05', 3, 'started', 25, 'some priority', 'assigned to someone', 'some history note', '2017-01-19 21:50:05', '2017-01-19 21:50:05', 1, 'last modified by someone'),
(21, 2, 'some phase', 'some task name23', 'some milestone', 'She Was Pretty is an awesome Kdrama', 2, '2017-01-31 21:50:29', '2017-02-03 21:50:29', 3, 'started', 25, 'some priority', 'assigned to someone', 'some history note', '2017-01-19 21:50:29', '2017-01-19 21:50:29', 1, 'last modified by someone'),
(22, 2, 'some phase', 'some task name24', 'some milestone', 'Legend of the Blue Sea', 2, '2017-02-04 21:50:48', '2017-02-10 21:50:48', 3, 'started', 25, 'some priority', 'assigned to someone', 'some history note', '2017-01-19 21:50:48', '2017-01-19 21:50:48', 1, 'last modified by someone'),
(23, 2, 'some phase', 'some task name25', 'some milestone', 'Moon Lovers Scarelet Heart Ryeo', 2, '2017-02-10 21:51:14', '2017-02-13 21:51:14', 3, 'started', 25, 'some priority', 'assigned to someone', 'some history note', '2017-01-19 21:51:14', '2017-01-19 21:51:14', 1, 'last modified by someone'),
(24, 2, 'some phase', 'some task name25', 'some milestone', 'Weightlifting fairy Kim Bok Joo', 2, '2017-02-13 21:51:31', '2017-02-17 21:51:31', 3, 'started', 25, 'some priority', 'assigned to someone', 'some history note', '2017-01-19 21:51:31', '2017-01-19 21:51:31', 1, 'last modified by someone'),
(25, 2, 'some phase', 'some task name26', 'some milestone', 'Jealousy Incarnate', 2, '2017-01-19 21:51:53', '2017-01-19 21:51:53', 3, 'started', 25, 'some priority', 'assigned to someone', 'some history note', '2017-01-19 21:51:53', '2017-01-19 21:51:53', 1, 'last modified by someone'),
(26, 2, 'some phase', 'some task name27', 'some milestone', 'Oh my Ghostess', 2, '2017-01-19 21:52:12', '2017-01-19 21:52:12', 3, 'started', 25, 'some priority', 'assigned to someone', 'some history note', '2017-01-19 21:52:12', '2017-01-19 21:52:12', 1, 'last modified by someone'),
(27, 2, 'some phase', 'some task name27', 'some milestone', 'Its Okay, It is love', 2, '2017-01-19 21:52:31', '2017-01-19 21:52:31', 3, 'started', 25, 'some priority', 'assigned to someone', 'some history note', '2017-01-19 21:52:31', '2017-01-19 21:52:31', 1, 'last modified by someone'),
(28, 2, 'some phase', 'some task name28', 'some milestone', 'Time Renegades', 2, '2017-01-19 21:52:46', '2017-01-19 21:52:46', 3, 'started', 25, 'some priority', 'assigned to someone', 'some history note', '2017-01-19 21:52:46', '2017-01-19 21:52:46', 1, 'last modified by someone'),
(29, 2, 'some phase', 'some task name29', 'some milestone', 'My Sassy Girl', 2, '2017-01-19 21:53:02', '2017-01-19 21:53:02', 3, 'started', 25, 'some priority', 'assigned to someone', 'some history note', '2017-01-19 21:53:02', '2017-01-19 21:53:02', 1, 'last modified by someone'),
(30, 2, 'some phase', 'some task name30', 'some milestone', 'The Innocent Man', 2, '2017-01-19 21:53:19', '2017-01-19 21:53:19', 3, 'started', 25, 'some priority', 'assigned to someone', 'some history note', '2017-01-19 21:53:19', '2017-01-19 21:53:19', 1, 'last modified by someone');

-- --------------------------------------------------------

--
-- Table structure for table `task_status`
--

CREATE TABLE `task_status` (
  `record_id` int(11) NOT NULL,
  `task_status_description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `task_status`
--

INSERT INTO `task_status` (`record_id`, `task_status_description`) VALUES
(1, 'Not Started'),
(2, 'In Progress'),
(3, 'Done'),
(4, 'Delayed'),
(5, 'Cancelled');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `firstname`, `lastname`) VALUES
(1, 'Jhinrie', 'Lagunsad'),
(2, 'Charmaine', 'Sorila');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`project_id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`task_id`),
  ADD KEY `project_id` (`project_id`),
  ADD KEY `record_owner` (`user_id`),
  ADD KEY `task_status` (`task_status`);

--
-- Indexes for table `task_status`
--
ALTER TABLE `task_status`
  ADD PRIMARY KEY (`record_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `project_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `task_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `task_status`
--
ALTER TABLE `task_status`
  MODIFY `record_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tasks`
--
ALTER TABLE `tasks`
  ADD CONSTRAINT `tasks_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`),
  ADD CONSTRAINT `tasks_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
