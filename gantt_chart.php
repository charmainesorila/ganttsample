 <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('#loadProject').click(function(){
			alert("ASdf");
			jQuery("#chart-settings").hide();
			jQuery("#gantt-chart-proper").show();
			
			google.charts.load('current', {'packages':['gantt']});
			google.charts.setOnLoadCallback(drawChart);
			function daysToMilliseconds(days) {
			  return days * 24 * 60 * 60 * 1000;
			}
			
			function drawChart() {
				data = {projectId : 1};
				$.ajax({
				type: "POST",
				dataType: "json",
				url: "gantt_chart_values.php", 
				data: data,
				success: function(jsonData) {
					
					var data = new google.visualization.DataTable();
					data.addColumn('string', 'Task ID');
					data.addColumn('string', 'Task Name');
					data.addColumn('string', 'Resource');
					data.addColumn('date', 'Start Date');
					data.addColumn('date', 'End Date');
					data.addColumn('number', 'Duration');
					data.addColumn('number', 'Percent Complete');
					data.addColumn('string', 'Dependencies');
					
					$.each( jsonData, function( index, val ) {
						var dateObj = new Date(val[3]);
						var month = dateObj.getUTCMonth() + 1; //months from 1-12
						var day = dateObj.getUTCDate();
						var year = dateObj.getUTCFullYear();
						var newdate = year + "/" + month + "/" + day;
						val[3] = new Date(year,month,day);
						
						var dateObj = new Date(val[4]);
						var month = dateObj.getUTCMonth() + 1; //months from 1-12
						var day = dateObj.getUTCDate();
						var year = dateObj.getUTCFullYear();
						var newdate = year + "/" + month + "/" + day;
						val[4] = new Date(year,month,day);
						
						val[5] = parseInt(val[5]);
						val[6] = parseInt(val[6]);
					});
					
					var array = $.map(jsonData, function(value, index) {
						return [value];
					});

					console.log(array);
					data.addRows(array);
					/*
					data.addRows([
					['2014Spring', 'Spring 2014', 'spring',
					 new Date(2014, 2, 22), new Date(2014, 5, 20), null, 100, null],
					['2014Summer', 'Summer 2014', 'summer',
					 new Date(2014, 5, 21), new Date(2014, 8, 20), null, 100, null],
					['2014Autumn', 'Autumn 2014', 'autumn',
					 new Date(2014, 8, 21), new Date(2014, 11, 20), null, 100, null],
					['2014Winter', 'Winter 2014', 'winter',
					 new Date(2014, 11, 21), new Date(2015, 2, 21), null, 100, null],
					['2015Spring', 'Spring 2015', 'spring',
					 new Date(2015, 2, 22), new Date(2015, 5, 20), null, 50, null],
					['2015Summer', 'Summer 2015', 'summer',
					 new Date(2015, 5, 21), new Date(2015, 8, 20), null, 0, null],
					['2015Autumn', 'Autumn 2015', 'autumn',
					 new Date(2015, 8, 21), new Date(2015, 11, 20), null, 0, null],
					['2015Winter', 'Winter 2015', 'winter',
					 new Date(2015, 11, 21), new Date(2016, 2, 21), null, 0, null],
					['Football', 'Football Season', 'sports',
					 new Date(2014, 8, 4), new Date(2015, 1, 1), null, 100, null],
					['Baseball', 'Baseball Season', 'sports',
					 new Date(2015, 2, 31), new Date(2015, 9, 20), null, 14, null],
					['Basketball', 'Basketball Season', 'sports',
					 new Date(2014, 9, 28), new Date(2015, 5, 20), null, 86, null],
					['Hockey', 'Hockey Season', 'sports',
					 new Date(2014, 9, 8), new Date(2015, 5, 21), null, 89, null]
					]);
					*/
					var options = {
					height: 900,
					gantt: {
					  trackHeight: 30
					},
					hAxis: {
						format: 'M/d/yy'
					  },
					};

					var chart = new google.visualization.Gantt(document.getElementById('chart_div'));

					chart.draw(data, options);
					
				},
				error: function(data){
					alert("error");
				}
				});
				
			}
			
			
		});
	})
    
  </script>
  <div id="chart_div">
  
  </div>
  
  
  
  
  
