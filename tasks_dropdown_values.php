<?php
	include "dbConnect.php";
	$query = "select column_name from information_schema.columns where table_name='tasks'";
	$result = $conn->query($query);
	$row = $result->fetch_assoc();
	$dropdown_val = [];
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			array_push($dropdown_val,$row['column_name']);
		}
	}
	echo json_encode($dropdown_val);
?>