
<html>
	<head>
		<title>Sample Project</title>
		<?php
		include "headings_import.php";
		?>
		<script>
		jQuery(document).ready(function(){
			 $('#tasks-table').DataTable();
		})
		</script>
	</head>
	<body>
		<table id="tasks-table">
			<thead>
				<tr>
					<th>TASK ID</th>
					<th>PROJECT ID</th>
					<th>TASKNAME</th>
					<th>DESCRIPTION</th>
					<th>START DATE</th>
					<th>END DATE</th>
					<th>TASK STATUS</th>
					<th>PERCENT COMPLETE</th>
					<th>ASSIGNED TO</th>
					<th>LAST MODIFIED BY</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th>TASK ID</th>
					<th>PROJECT ID</th>
					<th>TASKNAME</th>
					<th>DESCRIPTION</th>
					<th>START DATE</th>
					<th>END DATE</th>
					<th>TASK STATUS</th>
					<th>PERCENT COMPLETE</th>
					<th>ASSIGNED TO</th>
					<th>LAST MODIFIED BY</th>
				</tr>
			</tfoot>
			<tbody>
			<?php
			$query = "SELECT * FROM tasks";
			$result = $conn->query($query);
			if ($result->num_rows > 0) {
				while($row = $result->fetch_assoc()) {
					
					echo "<tr>";
					echo "<td>";
					echo $row['task_id'];
					echo "</td>";
					echo "<td>";
					echo $row['project_id'];
					echo "</td>";
					echo "<td>";
					echo $row['task_name'];
					echo "</td>";
					echo "<td>";
					echo $row['description'];
					echo "</td>";
					echo "<td>";
					echo $row['start_date'];
					echo "</td>";
					echo "<td>";
					echo $row['end_date'];
					echo "</td>";
					echo "<td>";
					echo $row['task_status'];
					echo "</td>";
					echo "<td>";
					echo $row['percent_complete'];
					echo "</td>";
					echo "<td>";
					echo $row['assigned_to'];
					echo "</td>";
					echo "<td>";
					echo $row['last_modified_by'];
					echo "</td>";
					echo "</tr>";
				}
			} else {
				echo "0 results";
			}
			$conn->close();
			?>
			</tbody>
		</table>
		
	</body>
</html>